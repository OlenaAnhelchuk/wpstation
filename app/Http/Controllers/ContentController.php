<?php

namespace App\Http\Controllers;

use App\Content;
use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plugins = Content::where([['user_id', '=', auth()->user()->id], ['type', '=', 'plugin']])->orderBy('name')->get();
        $themes = Content::where([['user_id', '=', auth()->user()->id], ['type', '=', 'theme']])->orderBy('name')->get();
        $plugin_files = File::where([['user_id', '=', auth()->user()->id], ['type', '=', 'plugin']])->orderBy('name')->get();
        $theme_files = File::where([['user_id', '=', auth()->user()->id], ['type', '=', 'theme']])->orderBy('name')->get();

        return view('settings.index', compact('plugins', 'themes', 'plugin_files', 'theme_files'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request->input('validateAs') == 'url') {
            $request->validate([
                'name' => 'string',
                'path' => 'url|regex:/.+\.zip$/|unique:contents,path',
                'type' => 'string|in:plugin,theme'
            ]);
        } elseif ($request->input('validateAs') == 'file') {
            $request->validate([
                'name' => 'string',
                'file' => 'file|mimes:zip',
                'type' => 'string|in:plugin,theme'
            ]);
        } else {
            $request->validate([
                'name' => 'string',
                'path' => 'string|unique:contents,path',
                'type' => 'string|in:plugin,theme'
            ]);
        }


        if ($request->file('file')) {
            $file = $request->file('file');
//            dd($file);
            $ext = $file->getClientOriginalExtension();


//            if (Storage::putFileAs('/public/plugins/', $file, $request['name'] . '.' . $ext)) {

                $storage_file = Storage::disk('local')->put('public/plugins',$request->file('file'));

                    if($storage_file != null){
                        $f = File::create([
                            'user_id' => auth()->user()->id,
                            'name' => $request->name,
                            'file' => $this->getURLFromFile($storage_file),
                            'type' => $request->type,
                            'extension' => $ext
                        ]);

                        $request->path = $this->getURLFromFile($storage_file);
                    }

//            }
        }

//        if (isset($upload) && $upload) {
//            $request->path = asset('storage/plugins/' . $upload->name . '.' . $upload->extension);
//        }

//        dd(auth()->user()->id);
        $post = Content::create([
            'user_id' => auth()->user()->id,
            'type' => $request->type,
            'name' => $request->name,
            'path' => $request->path,
        ]);

        if ($post) {
            return $post;
        }

        return json_decode(false);
    }

    private function getURLFromFile($file){
        $parts = explode("/",$file);
        array_shift($parts);
        $place = implode("/",$parts);
        return env('APP_URL')."/storage/".$place;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id.
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (Content::destroy($id)) {
            return 1;
//            return redirect()->back();
        }
    }

    public function search($type, $string)
    {
        $saved_data = [];
        if ($type == 'plugin') {
            $url = 'http://api.wordpress.org/plugins/info/1.1/';
            $action = 'query_plugins';
            $saved_data = Content::where('type', '=', 'plugin')->select('path')->get();
        }
        if ($type == 'theme') {
            $url = 'http://api.wordpress.org/themes/info/1.1/';
            $action = 'query_themes';
            $saved_data = Content::where('type', '=', 'theme')->select('path')->get();
        }

        if (!$url) {
            return '';
        }

        $array_data = [];

        if (count($saved_data)) {
            foreach ($saved_data as $obj) {
                $array_data[] = $obj->path;
            }
        }

        $page = 1;
        $req = (object)[];
        $req->per_page = 30;
        $req->search = $string;
        $data = array('action' => $action, 'request' => $req);

        $postdata = http_build_query(
            $data
        );

        $curl = curl_init($url . '?' . $postdata);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        $json = json_decode($result);

        if ($json) {
            $posts = [];
            if (isset($json->plugins)){
                $posts = $json->plugins;
            }
            if (isset($json->themes)){
                $posts = $json->themes;
            }
            $i = 0;
            foreach ($posts as $post) {
                if (in_array($post->slug, $array_data)) {
                    $posts[$i]->installed = true;
                }
                $i += 1;
            }
            return $posts;
        }
        return '';
    }


}
