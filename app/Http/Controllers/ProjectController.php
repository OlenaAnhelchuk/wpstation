<?php

namespace App\Http\Controllers;

use App\Content;
use App\Mail\WPInfo;
use App\Project;
use Illuminate\Http\Request;
use Collective\Remote\RemoteFacade as SSH;
use Illuminate\Support\Facades\Mail;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::where('user_id', '=', auth()->user()->id)->orderBy('sub_domain')->get();
        return view('project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $plugins = Content::where('user_id', '=', auth()->user()->id)->get();
//        $plugins = $content->where('type', '=', 'plugin')->pluck('id');
//        $themes = $content->where('type', '=', 'theme')->pluck('id');
        return view('project.create', compact('plugins'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'sub_domain_slug' => 'required|string|unique:projects,sub_domain',
            'database_slug' => 'required|string|unique:projects,database',
            'wp_admin' => 'email|nullable',
            'status' => 'integer|nullable'
        ]);

        $domain = $request->sub_domain_slug;
        $database = $request->database_slug;
        $host = $request->getHost();
        $email = $request->wp_admin ? $request->wp_admin : auth()->user()->email;


        $project = Project::create([
            'user_id' => auth()->user()->id,
            'sub_domain' => $domain,
            'database' => $database,
            'wp_admin' => $email,
            'status' => 1
        ]);

        if ($project) {
            $pass = str_random(12);

            $commands = [];
            $commands[] = storage_path('app/public/db.sh ' . $domain . ' ' . $database . '');
            $commands[] = storage_path('app/public/wp.sh ' . $domain . ' ' . $host . ' ' . $email . ' ' . $database . ' ' . $pass . '');
            $commands[] = storage_path('app/public/makesite.sh ' . $domain . ' ' . $host . '');
            $commands[] = storage_path('app/public/ensite.sh ' . $domain . '');

            $log = 'SSH logs:' . PHP_EOL;
            ob_start();
            SSH::run($commands, function ($line) {
                echo $line . PHP_EOL;
            });
            $log .= ob_get_clean();
            if (strpos($log, 'Success: WordPress installed successfully.') !== false && strpos($log, 'Enabling site') !== false):
                Mail::to($project->wp_admin)->send(new WPInfo($project->sub_domain.'.'.request()->getHost(), $pass));
                $response = (object)array(
                    'project' => $project,
                    'message' => (object)['status' => 'success', 'info' => 'WordPress installed successfully.'],
                    'log' => $log,
                    'success' => true
                );
            else:
                SSH::run([storage_path('app/public/dissite.sh ' . $project->sub_domain . '')]);
                $this->destroy($project);
                $response = (object)array(
                    'message' => (object)['status' => 'fail', 'info' => 'WordPress have not been installed.'],
//                    'log' => $log,
                    'success' => false
                );
            endif;

            return json_encode($response);

        }

        return '0';

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $request->validate([
            'status' => 'integer|nullable'
        ]);
        $project->status = $request->status;
        if ($project->save()) {
            if ($project->status == 1) {
                SSH::run([storage_path('app/public/ensite.sh ' . $project->sub_domain . '')]);
            } else {
                SSH::run([storage_path('app/public/dissite.sh ' . $project->sub_domain . '')]);
            }
            return $request->status;
        }
        return '0';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        SSH::run([storage_path('app/public/delete.sh ' . $project->sub_domain . ' ' . $project->database . '')]);
        Project::destroy($project->id);
        return 1;
    }

    public function restartServer()
    {
        SSH::run([storage_path('app/public/restart.sh')]);
        return 0;
    }

    public function setRights()
    {
        $domain = Project::where('user_id', '=', auth()->user()->id)->orderBy('id', 'DESC')->pluck('sub_domain')->first();
        SSH::run([storage_path('app/public/setrights.sh ' . $domain . '')], function ($line){
            echo $line.PHP_EOL;
        });
        return 1;
    }

    public function install(Request $request)
    {
        $domain = Project::where('user_id', '=', auth()->user()->id)->orderBy('id', 'DESC')->pluck('sub_domain')->first();

        $content = Content::where('id', '=', $request->id)->first();
        $commands = [];
        if ($content->type == 'plugin') {
            $commands[] = storage_path('app/public/wpplugin.sh ' . $domain . ' ' . $content->path . '');
        }
        if ($content->type == 'theme') {
            $commands[] = storage_path('app/public/wptheme.sh ' . $domain . ' ' . $content->path . '');
        }

        $log = 'SSH logs:' . PHP_EOL;
        ob_start();
        SSH::run($commands, function ($line) {
            echo $line . PHP_EOL;
        });
        $log .= ob_get_clean();
        if (strpos($log, 'Success:') !== false):
            $response = (object)array(
                'content' => $content,
                'message' => (object)['status' => 'success', 'info' => $content->name . ' installed.'],
//                'log' => $log,
                'success' => true
            );
        else:
            $response = (object)array(
                'message' => (object)['status' => 'fail', 'info' => $content->name . ' have not been installed.'],
//                'log' => $log,
                'success' => false
            );
        endif;

        return json_encode($response);
    }
}
