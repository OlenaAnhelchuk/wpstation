<?php

namespace App\Http\Middleware;

use Closure;

class DomainSlug
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $input = $request->all();
        if (isset($input['sub_domain'])):
            $input['sub_domain_slug'] = str_slug($input['sub_domain'], '-');
            $input['database_slug'] = str_slug($input['sub_domain'], '');
            $request->replace($input);
        endif;
        return $next($request);
    }
}
