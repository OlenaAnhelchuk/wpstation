<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WPInfo extends Mailable
{
    use Queueable, SerializesModels;

    public $hostname;
    public $pass;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($hostname, $pass)
    {
        $this->hostname = $hostname;
        $this->pass = $pass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.info')->subject('WP project')->with(['hostname'=>$this->hostname, 'pass'=>$this->pass]);
    }
}
