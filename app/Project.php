<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'user_id', 'sub_domain', 'wp_admin', 'database', 'status'
    ];
}
