<!doctype html>
<P St lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invitation from WPStation</title>
    <style>
    </style>
</head>

<p>We make WP installation <a href="http://{{$hostname}}/wp-login.php" target="_blank"><b>{{$hostname}}</b></a> for you</p>
<h5>Credentials:</h5>
<p>User name: <b>admin</b></p>
<p>Password: <b>{{ $pass }}</b></p>

<br><br>
Forever yours<br>
WP Station<br>

</body>
</html>