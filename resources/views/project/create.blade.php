@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <div class="card">
                    <div class="card-header">
                        <h1>Create project</h1>
                    </div>
                    <div class="card-body">
                        <project-new :plugins="{{ $plugins }}" :domain="'{{request()->getHost()}}'"></project-new>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
