@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="float-left">Projects</h4>
                        <div class="float-right">
                            <a class="btn btn-success btn-lg" href="projects/create">New Subdomain</a>
                        </div>
                    </div>

                    <project-list :posts="{{ $projects }}" :domain="'{{request()->getHost()}}'"></project-list>

                </div>
            </div>
        </div>
    </div>
@endsection
