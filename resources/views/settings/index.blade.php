@extends('layouts.app')

@section('content')
    <div class="container">
        <settings-tabs :plugins="{{ $plugins }}" :themes="{{ $themes }}" :plugin_files="{{ $plugin_files }}" :theme_files="{{ $theme_files }}"></settings-tabs>
    </div>
@endsection
