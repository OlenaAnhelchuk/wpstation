<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (auth()->user()){
        return redirect('/projects');
    }
    return redirect('/login');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('projects/create', 'ProjectController@create')->middleware('auth');
Route::resource('projects', 'ProjectController')->middleware(['auth', 'domain']);
Route::resource('settings', 'ContentController')->middleware('auth');
Route::post('apache', 'ProjectController@restartServer')->middleware('auth');
Route::post('install/rights', 'ProjectController@setRights')->middleware('auth');
Route::post('install', 'ProjectController@install')->middleware('auth');
Route::get('/search/{type}/{str}', 'ContentController@search');
