<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpclidemo' );

/** MySQL database username */
define( 'DB_USER', 'homestead' );

/** MySQL database password */
define( 'DB_PASSWORD', 'secret' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'x__w~TqUFIHDU*ggoh^v.mLBGbH21>/#a;@itR;EH]5@I5Z/V V&NK(?7vtsM$rv' );
define( 'SECURE_AUTH_KEY',   'hejstWxv>X])bb]HYRk63Ni8[aCVfs!=3,H.lB87EO?Hf&T_=#`jjY)v!3{L19Hx' );
define( 'LOGGED_IN_KEY',     'lLE`_flr<!]Y6{[}rvF$aSL&]~0IW^4N4J&vd_jj.69nY73B<Q|A1z`8S/ `JO^B' );
define( 'NONCE_KEY',         'Po:Y3_WYEfGuqj0Ls$6[lFSg [u/v/I<A=v0O$VN:PeYd|<-Ml.V?g(;(9ldZZI~' );
define( 'AUTH_SALT',         '<mb%dgv_EX}`9B~X;.`AM5hXo*q]xoJfKRvi_#4#g-uz_(gLn-(XT.BC)n6F]ts?' );
define( 'SECURE_AUTH_SALT',  'a_b<7H5Ji/VUt<Hs`Qt1U3]l6XmbV*,N]Y$U/(mYS@HqhZIK93UU`_6b:LHNiD33' );
define( 'LOGGED_IN_SALT',    'YGmE~q`C*;j/vi7v8@=y5Vw@-k`X%C~q^Im &8w|85>miq!Y9~LYS^t7?sJ91vI*' );
define( 'NONCE_SALT',        '`Tf[3_0!1:B5OJ{?npYR2zFZN,-[5MR=UgC=HfU,Mbh(Q2mgk`tvpmruhnr1>v^k' );
define( 'WP_CACHE_KEY_SALT', 'aJ10{MRq#2o7Qf%R|mKagLI}vENhx|d/m[?{q~z`8eBbvhLX45]{7@|c6bdaR*kM' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
